const mongoose = require('mongoose');
const { Schema } = mongoose;

const reportSchema = new Schema({
    student_id: {
        type: String,
        required: true
    },
    teacher_id: {
        type: String,
        required: true        
    },
    isPresent: {
        type: Boolean,
        required: true,
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    date: {
        type: Number,
        required: true,
    }
});
const Report = mongoose.model('report', reportSchema);
module.exports = Report;