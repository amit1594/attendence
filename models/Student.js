const mongoose = require('mongoose');
const { Schema } = mongoose;

const studentSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true,
    },
    contact_number: {
        type: String,
        required: true,
    },
    roll_number: {
        type: String,
        required: true
    },
    teacher_id: {
        type: String,
        required: true
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    date: {
        type: Date,
        default: Date.now
    }
});
const Student = mongoose.model('student', studentSchema);
module.exports = Student;