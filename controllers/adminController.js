const express = require("express");
const router = express.Router();
const Admin = require("../models/Admin")

var jwt = require("jsonwebtoken");

// we need to kept this JWT_SECRET in environment vairable but kept here because this website is only for demo.
const JWT_SECTRT = "amitisaplayer";

router.post("/get_admin_detail", async (req, res) => {
  try {
    const decodedToken = jwt.verify(req.body.token, JWT_SECTRT);
    const get_admin_detail = await Admin.findOne({email: decodedToken.id});
    res.status(200).json({
      error: false,
      message: "Admin Data Fetched Successfully",
      data: get_admin_detail,
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

module.exports = router;
