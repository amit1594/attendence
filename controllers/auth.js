const express = require("express");
const router = express.Router();
const { body, validationResult } = require("express-validator");
const Admin = require("../models/Admin")
const Teacher = require("../models/Teacher")
var jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const JWT_SECTRT = "amitisaplayer";

// Admin Signup
router.post(
  "/admin_signup",
  [
    body("name", "Enter a valid name").isLength({ min: 3 }),
    body("email", "Enter a valid email")
      .notEmpty()
      .withMessage("Email is required")
      .isEmail()
      .withMessage("Invalid email format"),
    body("password").isLength({ min: 8 }),
    body("confirmpassword").isLength({ min: 8 }),
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res
          .status(400)
          .json({ error: true, message: errors.array()[0].msg });
      }
      const data = req.body;
      const user = await Admin.findOne({email: req.body.email});
      if (user) {
        return res.status(400).json({
          error: true,
          message: "Sorry a amin with this email already exists",
        });
      }
      if (req.body.password != req.body.confirmpassword) {
        return res
          .status(400)
          .json({ 
            error: true, 
            message: "Sorry! Password is not matched" 
          });
      }
      const salt = await bcrypt.genSalt(10);
      secPass = await bcrypt.hash(req.body.password, salt);
      const adminDetails = await Admin.create({
        name: req.body.name,
        email: req.body.email,
        isAdmin: true,
        password: secPass,
      });
      res
        .status(200)
        .json({ error: false, message: "Register Successfully", data: adminDetails });
    } catch (error) {
      console.error(error.message);
      res
        .status(500)
        .send({ error: true, message: "internal server Error occured" });
    }
  }
);

// Teacher Signup
router.post(
  "/teacher_signup",
  [
    body("name", "Enter a valid name").isLength({ min: 3 }),
    body("email", "Enter a valid email")
      .notEmpty()
      .withMessage("Email is required")
      .isEmail()
      .withMessage("Invalid email format"),
    body("specialist", "Enter a valid specialist").isLength({ min: 3 }),
    body("contact_number", "Enter a valid contact_number").isLength({ min: 10 }),
    body("password").isLength({ min: 8 }),
    body("confirmpassword").isLength({ min: 8 }),
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res
          .status(400)
          .json({ error: true, message: errors.array()[0].msg });
      }
      const data = req.body;
      const user = await Teacher.findOne({email: req.body.email});
      if (user) {
        return res.status(400).json({
          error: true,
          message: "Sorry a teacher with this email already exists",
        });
      }
      if (req.body.password != req.body.confirmpassword) {
        return res
          .status(400)
          .json({ 
            error: true, 
            message: "Sorry! Password is not matched" 
          });
      }
      const salt = await bcrypt.genSalt(10);
      secPass = await bcrypt.hash(req.body.password, salt);
      const teacherDetails = await Teacher.create({
        name: req.body.name,
        email: req.body.email,
        specialist: req.body.specialist,
        contact_number: req.body.contact_number,
        password: secPass,
      });
      res
        .status(200)
        .json({ error: false, message: "Register Successfully", data: teacherDetails });
    } catch (error) {
      console.error(error.message);
      res
        .status(500)
        .send({ error: true, message: "internal server Error occured" });
    }
  }
);

router.post(
  "/login",
  [
    body("email", "Enter valid Email").isEmail(),
    body("password", "Password cannot be blank").exists(),
  ],
  async (req, res) => {
    const data = req.body;
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json({ error: true, message: errors.array()[0].msg });
    }

    try {
      let user;
      if(data.isAdmin)
      {
        user = await Admin.findOne({email: data.email});
      }
      else
      {
        user = await Teacher.findOne({email: data.email});
      }
      if (!user) {
        success = false;
        return res.status(400).json({
          error: true,
          message: "Please try to login with correct credential",
        });
      }
      const passwordCompare = await bcrypt.compare(
        data.password,
        user.password
      );
      if (!passwordCompare) {
        success = false;
        return res.status(400).json({
          error: true,
          message: "Please try to login with correct password",
        });
      }
      const data1 = {
        id: user.email,
        admin: user.isAdmin,
      };
      const authtoken = jwt.sign(data1, JWT_SECTRT);
      res.json({
        error: false,
        authtoken: authtoken,
        email: user.email,
        isAdmin: user.isAdmin,
        message: "Login Succesfully",
      });
      // res.status(success).json(authtoken);
    } catch (error) {
      console.error(error.message);
      res.status(500).send({
        error: true,
        error_message: error,
        message: "internal server Error occured",
      });
    }
  }
);

router.post("/get_id_by_token", async (req, res) => {
  try {
    const decodedToken = jwt.verify(req.body.authtoken, JWT_SECTRT);
    let user = await database.query(
      `SELECT name FROM car_admin WHERE email='${decodedToken.id}'`
    );
    console.log(user[0][0].name, "user");
    res.json({
      error: false,
      id: decodedToken.id,
      name: user[0][0].name,
      isAdmin: decodedToken.admin,
    });
    // res.status(success).json(authtoken);
  } catch (error) {
    console.error(error.message);
    res.status(500).send({
      error: true,
      error_message: error,
      message: "internal server Error occured",
    });
  }
});

module.exports = router;
