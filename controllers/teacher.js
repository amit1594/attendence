const express = require("express");
const router = express.Router();
const Student = require("../models/Student")
const Report = require("../models/Report")
const Teacher = require("../models/Teacher")
const { body, validationResult } = require("express-validator");
const JWT_SECTRT = "amitisaplayer";
var jwt = require("jsonwebtoken");

router.post("/create_new_student", async (req, res) => {
  try {
    const data = req.body;
    const decodedToken = jwt.verify(req.body.teacher_id, JWT_SECTRT);
    const studentDetails = await Student.create({
        name: data.name,
        address: data.address,
        contact_number: data.contact_number,
        roll_number: data.roll_number,
        teacher_id: decodedToken.id
    })
    res.status(200).json({
      error: false,
      message: "Student Created Successfully",
      data: studentDetails,
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.post("/update_attendence", async (req, res) => {
  try {
    const data = req.body;
    console.log(data, "data");
    const decodedToken = jwt.verify(data.token, JWT_SECTRT);
    const studentReport = await Student.findOne({student_id: data.student_id, teacher_id: decodedToken.id, date: data.day})
    if(studentReport)
    {
        studentReport.isPresent = data.changeAttendence;
        await studentReport.save();
    }
    else
    {
        await Report.create({
            student_id: data.student_id, 
            teacher_id: decodedToken.id, 
            isPresent: data.changeAttendence=="Present" ? true : false,
            date: data.day
        })
    }
    res.status(200).json({
      error: false,
      message: "Student Created Successfully"
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.post("/get_teacher_detail", async (req, res) => {
  try {
    const decodedToken = jwt.verify(req.body.token, JWT_SECTRT);
    let get_teacher_detail = await Teacher.findOne({email: decodedToken.id});
    res.status(200).json({
      error: false,
      message: "Teacher Data Fetched Successfully",
      data: get_teacher_detail,
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

// router.post("/get_all_teacher", async (req, res) => {
//   try {
//     const get_teacher_detail = await Teacher.find({});
    
//     console.log(get_teacher_detail, "get_teacher_detail");
//     res.status(200).json({
//       error: false,
//       message: "Teacher Data Fetched Successfully",
//       data: detail,
//     });
//   } catch (error) {
//     console.error(error.message);
//     res
//       .status(500)
//       .send({ error: true, message: "internal server Error occured" });
//   }
// });

router.post("/get_attendence", async (req, res) => {
  try {
    const decodedToken = jwt.verify(req.body.token, JWT_SECTRT);
    let all_student = await Student.find({teacher_id: decodedToken.id});
    let student_id = {};
    for(let i=0;i<all_student.length;i++)
    {
        let one_attendence = [];
        for(let j=1;j<=31;j++)
        {
            let student_attendence = await Report.findOne({student_id: all_student[i]._id, teacher_id: decodedToken.id, date: j})
            if(!student_attendence)
            {
                one_attendence.push('N/A');
            }
            else if(student_attendence.isPresent)
            {
                one_attendence.push('P');
            }
            else
            {
                one_attendence.push('A');
            }
        }
        student_id[all_student[i]._id] = one_attendence;
    }
    res.status(200).json({
      error: false,
      message: "Teacher Data Fetched Successfully",
      data: student_id,
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.post("/get_student", async (req, res) => {
  try {
    const single_student = await Student.findOne({_id: req.body.id});
    res.status(200).json({
      error: false,
      message: "Student Data Fetched Successfully",
      data: single_student,
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.post("/delete_student", async (req, res) => {
  try {
    const decodedToken = jwt.verify(req.body.token, JWT_SECTRT);
    await Student.deleteOne({_id: req.body.student_id, teacher_id: decodedToken.id});
    await Report.deleteMany({student_id: req.body.student_id, teacher_id: decodedToken.id});
    res.status(200).json({
      error: false,
      message: "Student Record Deleted Successfully",
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.post("/update_student_detail", async (req, res) => {
  try {
    const data = req.body;
    console.log(data, "data");
    const student_detail = await Student.findOne({_id: req.body.student_id});
    student_detail.name = data.name.length>0 ? data.name : student_detail.name;
    student_detail.address = data.address.length>0 ? data.address : student_detail.address;
    student_detail.contact_number = data.contact_number.length>0 ? data.contact_number : student_detail.contact_number;
    student_detail.roll_number = data.roll_number.length>0 ? data.roll_number : student_detail.roll_number;
    await student_detail.save();
    res.status(200).json({
      error: false,
      message: "Student Record Updated Successfully",
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

module.exports = router;
